﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace AbstractFactory
{
    class AbstractFactoryDemo
    {

        public static void Main(string[] args)
        {
            AbstractFabrika çf = FabrikaÜreticisi.FabrikaÜret("çay");
            Çay sç = çf.ÇayÜret("soğuk");
            sç.İç();
            AbstractFabrika şf = FabrikaÜreticisi.FabrikaÜret("şeker");
            Şeker tş = şf.ŞekerÜret("toz");
            tş.Karıştırıl();
        }
    }
    interface Çay
    {
        void İç();
    }

    class SıcakÇay : Çay
    {
        public void İç()
        {
            Console.WriteLine("Sıcak Çay içildi");
        }
    }

    class SoğukÇay : Çay
    {
        public void İç()
        {
            Console.WriteLine("Soğuk Çay içildi.");
        }
    }

    interface Şeker
    {
        void Karıştırıl();
    }

    class TozŞeker : Şeker
    {
        public void Karıştırıl()
        {
            Console.WriteLine("Tozşeker Karıştıldı.");
        }
    }

    class KüpŞeker : Şeker
    {
        public void Karıştırıl()
        {
            Console.WriteLine("Küp şeker karıştırıldı.");
        }
    }

    class ŞekerFabrikası : AbstractFabrika
    {
        public override Çay ÇayÜret(string Tip)
        {
            return null;
        }

        public override Şeker ŞekerÜret(string Tip)
        {

            if (string.IsNullOrEmpty(Tip))
            {
                return null;
            }
            else
            {
                Console.WriteLine("şeker fabrikası şeker üretiyor.");
                Tip = Tip.Trim().ToLower();
                if (Tip == "toz")
                {
                    return new TozŞeker();
                }
                else
                {
                    return new KüpŞeker();
                }
            }
        }
    }

    class ÇayFabrikası : AbstractFabrika
    {
        public override Çay ÇayÜret(string Tip)
        {
            if (string.IsNullOrEmpty(Tip))
            {
                return null;
            }
            else
            {
                Console.WriteLine("Çay fabrikası şeker üretiyor.");
                Tip = Tip.Trim().ToLower();
                if (Tip == "soğuk")
                {
                    return new SoğukÇay();
                }
                else
                {
                    return new SıcakÇay();
                }
            }
        }

        public override Şeker ŞekerÜret(string Tip)
        {
            return null;
        }
    }


    abstract class AbstractFabrika
    {
        public abstract Şeker ŞekerÜret(string Tip);
        public abstract Çay ÇayÜret(string Tip);
    }

    class FabrikaÜreticisi
    {
        public static AbstractFabrika FabrikaÜret(string Tip)
        {

            if (string.IsNullOrEmpty(Tip))
            {
                return null;
            }
            else
            {
                Console.WriteLine("Fabrika Kuruluyor");
                Tip = Tip.Trim().ToLower();
                if (Tip == "çay")
                {
                    return new ÇayFabrikası();
                }
                else
                {
                    return new ŞekerFabrikası();
                }
            }
        }
    }

}
