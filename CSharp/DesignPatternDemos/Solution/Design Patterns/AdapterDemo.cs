﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adapter
{

    class Sıvı
    {
        public void KapağınıAç()
        {
            Console.WriteLine("Kapak açıldı.");
        }

        public void İç()
        {
            Console.WriteLine("Lıkır lıkır içiliyor.");
        }

        public void ÇöpeAt()
        {
            Console.WriteLine("Kapak kapatılıp çöpe atıldı.");
        }
    }


    class Katı
    {
        public void AmbalajıYırt()
        {
            Console.WriteLine("Ambalaj açıldı.");
        }

        public void Ye()
        {
            Console.WriteLine("Kütür kütür yenyior");
        }

        public void ÇöpeAt()
        {
            Console.WriteLine("Ambalaj katlanıp çöpe atıldı.");
        }
    }

    interface ITüketilebilir
    {
        void TüketmeyeBaşla();
        void Tüket();
        void ÇöpünüAt();
    }

    class SıvıAdaptör : ITüketilebilir
    {
        private Sıvı fieldSıvı;

        public SıvıAdaptör(Sıvı s)
        {
            this.fieldSıvı = s;
        }

        public void Tüket()
        {
            fieldSıvı.İç();
        }

        public void TüketmeyeBaşla()
        {
            fieldSıvı.KapağınıAç();
        }

        public void ÇöpünüAt()
        {
            fieldSıvı.ÇöpeAt();
        }
    }

    class KatıAdaptör : ITüketilebilir
    {
        private Katı fieldKatı;

        public KatıAdaptör(Katı k)
        {
            this.fieldKatı = k;
        }
        public void Tüket()
        {
            fieldKatı.Ye();
        }

        public void TüketmeyeBaşla()
        {
            fieldKatı.AmbalajıYırt();
        }

        public void ÇöpünüAt()
        {
            fieldKatı.ÇöpeAt();
        }
    }
    class AdapterDemo
    {

        public static void Main(string[] args)
        {
            Console.WriteLine("--------Bakkala Hoş geldiniz----------");
            while (true)
            {
                ITüketilebilir[] ÜrünTezgahı = new ITüketilebilir[2];
                ÜrünTezgahı[0] = new KatıAdaptör(new Katı());
                ÜrünTezgahı[1] = new SıvıAdaptör(new Sıvı());
                while (true)
                {
                    Console.WriteLine("Lütfen tüketilebilir ürün tipini seçiniz :");
                    Console.WriteLine("1. Yiyecek");
                    Console.WriteLine("2. İçecek");
                    string seçim = Console.ReadLine();
                    int seçimKodu;
                    if (int.TryParse(seçim , out seçimKodu) && (seçimKodu <= 2 && seçimKodu >= 1))
                    {
                        seçimKodu = seçimKodu - 1;
                        ÜrünTezgahı[seçimKodu].TüketmeyeBaşla();
                        ÜrünTezgahı[seçimKodu].Tüket();
                        ÜrünTezgahı[seçimKodu].ÇöpünüAt();
                    }
                }
                
            }
            
        }
    }
}
