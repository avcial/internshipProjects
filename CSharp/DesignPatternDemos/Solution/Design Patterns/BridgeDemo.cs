﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridge
{
    
        enum PulDurumu {PulVar,PulYok }
    interface IGönderebilir
    {
        void Gönder();
    }

    class PTT : IGönderebilir
    {
        private string GöndermeŞifresi;

        public PTT(string GöndermeŞifresi)
        {
            this.GöndermeŞifresi = GöndermeŞifresi;
        }
        public void Gönder()
        {
            Console.WriteLine("PTT ile Gönderildi.");
        }
    }

    class UPS : IGönderebilir
    {
        private string AracıBilgisi;
        private string TakipNumarası;
        public UPS(string AracıBilgisi, string TakipNumarası)
        {
            this.AracıBilgisi = AracıBilgisi;
            this.TakipNumarası = TakipNumarası;
        }

        public void Gönder()
        {
            Console.WriteLine("UPS ile gönderildi");
        }
    }

      abstract class Gönderilebilir
    {
        protected string AlıcıBilgisi;
        protected string GöndericiBilgisi;
        protected IGönderebilir Şirket;

        public virtual void Yolla()
        {
            Console.WriteLine("----Köprü Gönderim Sistemimimze hoşgeldiniz----");
            Console.WriteLine("Alıcı Bilgisi : {0}",AlıcıBilgisi);
            Console.WriteLine("Gönderici Bilgisi : {0}",GöndericiBilgisi);
            Şirket.Gönder();
        }



    }

    class Mektup : Gönderilebilir
    {
        private PulDurumu PulVarMı;
        public Mektup(IGönderebilir Şirket,string AlıcıBilgisi, string GöndericiBilgisi, PulDurumu PulVarMı)
        {
            this.PulVarMı = PulVarMı;
            this.Şirket = Şirket;
            this.AlıcıBilgisi = AlıcıBilgisi;
            this.GöndericiBilgisi = GöndericiBilgisi;
        }

        public override void Yolla()
        {
           
            base.Yolla();
            Console.WriteLine(this.PulVarMı);
        }
    }

    class Kargo : Gönderilebilir
    {
        private decimal Ağırlık;

        public Kargo(IGönderebilir Şirket, string AlıcıBilgisi, string GöndericiBilgisi,decimal Ağırlık )
        {
            this.Ağırlık = Ağırlık;
            this.Şirket = Şirket;
            this.AlıcıBilgisi = AlıcıBilgisi;
            this.GöndericiBilgisi = GöndericiBilgisi;
        }
        public override void Yolla()
        {
            
            base.Yolla();
            Console.WriteLine("Ağırlık :{0}", Ağırlık);
        }
    }

    class BridgeDemo
    {
        public static void Main(String[] args)
        {
            IGönderebilir PTT = new PTT("Memleketim");
            IGönderebilir UPS = new UPS("Ebay", "1234");

            Gönderilebilir Mektup = new Mektup(PTT, "Sen", "Ben", PulDurumu.PulVar);
            Gönderilebilir Kargo = new Kargo(UPS, "Gene Sen", "Gene Ben", 2312);
            List<Gönderilebilir> GönderilecekHerŞey = new List<Gönderilebilir>() { Mektup, Kargo };

            foreach (Gönderilebilir item in GönderilecekHerŞey)
            {
                item.Yolla();
            }
            

            Console.Read();
        }
    }

}
