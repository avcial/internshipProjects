﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Composite
{
    abstract class Çalışan
    {
        protected List<Çalışan> astları;
        protected string isim;
        public abstract void Yazdır();

        public Çalışan(string İsim)
        {
            this.isim = İsim;
            this.astları = new List<Çalışan>();
        }

        public void AstEkle(Çalışan ç)
        {
            this.astları.Add(ç);
        }
    }

    class Personel : Çalışan
    {
        public Personel(string İsim) : base(İsim)
        {
        }

        public override void Yazdır()
        {
            Console.WriteLine("-{0}", this.isim);
            foreach (Çalışan ç in this.astları)
            {
                Console.Write("-");
                ç.Yazdır();
            }
        }
    }

    public class Composite
    {
        public static void Main(string[] args)
        {
            Personel Yüzbaşı = new Personel("Yüzbaşı");
            Personel Onbaşı = new Personel("Onbaşı");
            Personel Er1 = new Personel("Er1");
            Personel Er2 = new Personel("Er2");

            Yüzbaşı.AstEkle(Onbaşı);
            Onbaşı.AstEkle(Er1);
            Onbaşı.AstEkle(Er2);
            Yüzbaşı.Yazdır();
            Console.ReadLine();
        }
    }

}
