﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facade
{
    class TurnikeKartı
    {
        public void TurnikedenGeç()
        {
            Console.WriteLine("Kart Alındı, Turnikeden Geçildi");
        }
    }

    class Asansör
    {
        public void KataÇık()
        {
            Console.WriteLine("Kata çıkıldı.");
        }
    }

    class Sandalye
    {
        public void YerineOtur()
        {
            Console.WriteLine("Sandalye alıp oturuldu");
        }
    }

    class Bilgisayar
    {
        public void BilgisayarıAç()
        {
            Console.WriteLine("Bilgisayar açıldı.");
        }
    }


    class Facade
    {
        TurnikeKartı tk;
        Asansör a;
        Sandalye s;
        Bilgisayar b;

        public Facade()
        {
            tk = new TurnikeKartı();
            a = new Asansör();
            s = new Sandalye();
            b = new Bilgisayar();
        }

        public void ÇalışmayaBaşla()
        {
            tk.TurnikedenGeç();
            a.KataÇık();
            s.YerineOtur();
            b.BilgisayarıAç();
        }
    }

    class FacadeDemo
    {
        static void Main(string[] args)
        {


            //Çalışmaya başlamak için gerekli adımları izle

            Console.WriteLine("Aşağıdaki işlemler için Main de 8 satır yazıldı.");
            TurnikeKartı TurnikeKartı = new TurnikeKartı();
            Asansör Asansör = new Asansör();
            Sandalye Sandalye = new Sandalye();
            Bilgisayar Bilgisayar = new Bilgisayar();
            TurnikeKartı.TurnikedenGeç();
            Asansör.KataÇık();
            Sandalye.YerineOtur();
            Bilgisayar.BilgisayarıAç();
            Console.WriteLine("");

            //Veya Facade ile sadece ÇalışmayaBaşla de
            Console.WriteLine("Aşağıdaki işlemler için ise Main de 2 satır yazıldı.");
            Facade facade = new Facade();
            facade.ÇalışmayaBaşla();

            Console.Read();
        }
    }

}

