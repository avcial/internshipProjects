﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Factory
{

    abstract class Ürün
    {
        protected string ürünAdı;
        public abstract void ÜrünüGetir();
    }

    class KatıÜrün : Ürün
    {
        public KatıÜrün(string ÜrünAdı)
        {
            this.ürünAdı = ÜrünAdı;
        }
        public override void ÜrünüGetir()
        {

            Console.WriteLine("Sana yemen için {0} getiriyorum.", this.ürünAdı);
        }
    }

    class SıvıÜrün : Ürün
    {
        public SıvıÜrün(string ÜrünAdı)
        {
            this.ürünAdı = ÜrünAdı;
        }
        public override void ÜrünüGetir()
        {
            Console.WriteLine("Sana içmen için {0} getiriyorum", this.ürünAdı);

        }
    }

    class VarsayılanÜrün : Ürün
    {
        public override void ÜrünüGetir()
        {
            Console.WriteLine("Kalmadı kardeş haftaya gelir ama.");
        }
    }

    class ÜrünFactory
    {
        public static Ürün ÜrünüYolla(string ÜrünTipi)
        {
            switch (ÜrünTipi.ToLower())
            {
                case "içecek":
                    Ürün s = new SıvıÜrün("Sprite");
                    return s;

                case "yiyecek":
                    Ürün k = new KatıÜrün("Ekmek");
                    return k;

                default:
                    Ürün v = new VarsayılanÜrün();
                    return v;
            }
        }
    }

    class FactoryDemo
    {
        public static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Hoşgeldin, nasıl bişey veriyim sana?");
                Console.Write("Ne tür birşey istediğinizi girin : ");
                Ürün Mal = ÜrünFactory.ÜrünüYolla(Console.ReadLine());
                Mal.ÜrünüGetir();

            }    
        }
    }

}

