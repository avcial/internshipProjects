﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlyWeight
{

    abstract class Taşıt
    {

        public Taşıt()
        {
            Console.WriteLine("{0} satın alındı.", this.GetType().Name);
            TaşıtBirimi.elimizdekiler.Add(this);
        }
        public abstract void Kullan();
    }

    class Araba : Taşıt
    {
        public override void Kullan()
        {
            Console.WriteLine("{0} kullanılıyor", this.GetType().Name);
        }
    }

    class Otobüs : Taşıt
    {
        public override void Kullan()
        {
            Console.WriteLine("{0} kullanılıyor", this.GetType().Name);
        }
    }

    enum TaşıtTipler
    {
        Araba,
        Otobüs
    }

    class TaşıtBirimi
    {
        public static List<Taşıt> elimizdekiler = new List<Taşıt>();

        public Taşıt Taşıtİste(TaşıtTipler tip)
        {
            switch (tip)
            {
                case TaşıtTipler.Araba:
                    if (elimizdekiler.Any(i=> i is Araba))
                    {
                        Console.WriteLine("Elimizde araba var");
                        return elimizdekiler.FirstOrDefault(i => i is Araba);
                    }
                    else
                    {
                        return new Araba();
                    }
                case TaşıtTipler.Otobüs:
                    if (elimizdekiler.Any(i => i is Otobüs))
                    {
                        Console.WriteLine("Elimizde araba var.");
                        return elimizdekiler.FirstOrDefault(i => i is Otobüs);
                    }
                    else
                    {
                        return new Otobüs();
                    }
                default:
                    return null;
            }
        }
    }
    class FlyWeightDemo
    {
        public static void Main(string[] args)
        {
            TaşıtBirimi tb = new TaşıtBirimi();

            while (true)
            {
                Console.WriteLine("Ne kullanmak istediğinizi giriniz! Araba için 0, Otobüs için 1");
                Console.Write("Tercih :");
                TaşıtTipler tercih;
                int s = Convert.ToInt32(Console.ReadLine());
                if (s> Enum.GetNames(typeof(TaşıtTipler)).Length)
                {
                    Console.WriteLine("Yalnış tercih tekrar seçin.");
                    continue;
                }
                

                    tercih = (TaşıtTipler)s;
                
                Taşıt istenen =    tb.Taşıtİste(tercih);
                istenen.Kullan();
            }
        }
    }
}
