﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mediator
{
    interface ISlack
    {
        void MesajGönder(string isim,string mesaj);
    }

    class KanalBir : ISlack
    {
        public void MesajGönder(string isim,string message)
        {
            Console.WriteLine("{0} -> {1} mesaj : {2}",isim, this.GetType().Name, message);
        }
    }

    class Kanalİki : ISlack
    {
        public void MesajGönder(string isim,string message)
        {
            
            Console.WriteLine("{0} -> {1} mesaj : {2}",isim, this.GetType().Name, message);
        }
    }

    class Personel
    {
        private string İsim { get; set; }
        private ISlack Kanal { get; set; }

        public Personel(ISlack Kanal)
        {
            this.Kanal = Kanal;
        }
        public void İletişimeGeç(string mesaj)
        {
            this.Kanal.MesajGönder(İsim, mesaj);
        }

    }
    class MediatorDemo
    {
        public  static  void Main(string[] args)
        {
            Personel Patron = new Personel(new KanalBir());
            Personel altKademe = new Personel(new Kanalİki());
            Patron.İletişimeGeç("Herkesi toplantı odasına toplayın.");
            altKademe.İletişimeGeç("Gene ne var acaba???");
        }
    }
}
