﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NullObject
{
    public abstract class Marka
    {
        protected string ÜrünAdi;
        public abstract void ÜrününAdınıSöyle();

    }
    public class DoluÜrün : Marka
    {
        public DoluÜrün(string Adı)
        {
            this.ÜrünAdi = Adı;
        }
        public override void ÜrününAdınıSöyle()
        {
            Console.WriteLine("Bu kutuda {0} ürününden var", this.ÜrünAdi);
        }
    }

   public class BoşÜrün : Marka
    {
        public override void ÜrününAdınıSöyle()
        {
            Console.WriteLine("Bu kutu boş!");
        }
    }

    public class BakkalDeposu
    {
        public static List<string> Depo = new List<string>() { "Fanta", "Kola", "Çekirdek", "Su" };
        public static Marka DepodanBişeyGetir(string ÜrününAdı)
        {
            foreach (string mal in Depo)
            {
                if (mal.Equals(ÜrününAdı,StringComparison.InvariantCultureIgnoreCase))
                {
                    return new DoluÜrün(mal);
                }
                
            }
            return new BoşÜrün();
        }
    }


    class NullObjectDemo
    {

        public static void Main(string[] args)
        {
            while (true)
            {
                string istenenÜrün;
                Console.Write("Depodan getirilecek ürün adını yazınız : ");
                istenenÜrün = Console.ReadLine();

                Marka m = BakkalDeposu.DepodanBişeyGetir(istenenÜrün);
                m.ÜrününAdınıSöyle();


            }

        }
    }
}
