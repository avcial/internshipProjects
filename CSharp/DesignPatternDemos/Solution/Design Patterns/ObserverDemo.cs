﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Observer
{
    abstract class Observable
    {
        protected int state;

        protected List<Observer> ObserverList = new List<Observer>();

       public abstract void AddObserver(Observer o);

       public abstract void UpdateState(int state);

    }


    class ConcreteObservable : Observable
    {

       
      public override void AddObserver(Observer o)
        {
            this.ObserverList.Add(o);
        }

      public override void UpdateState(int state)
      {
          this.state = state;
          
          foreach (Observer item in this.ObserverList)
          {
              item.Notify();
          }
      }
    }

    interface Observer
    {
        void Notify();
    }

    class ConcreteObserver : Observer
    {
        public void Notify()
        {
            Console.WriteLine("This Instance Has been notified");
        }
    }

    class ObserverDemo
    {
        static void Main(string[] args)
        {

            Observable observable = new ConcreteObservable();
            observable.AddObserver(new ConcreteObserver());
            observable.AddObserver(new ConcreteObserver());

            observable.UpdateState(5);

            Console.Read();
        }
    }
}
