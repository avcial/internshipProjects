﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prototype
{

    abstract class İnsanKalıbı
    {
        public string isim;
        public abstract İnsanKalıbı Klonla();
    }

    class Erkek : İnsanKalıbı
    {
        public override İnsanKalıbı Klonla()
        {
            Console.WriteLine("{0} isimli erkeğin klonu oluşturuluyor", this.isim);
            return new Erkek()
            {
                
                isim = this.isim
            };
        }
    }

    class Kadın : İnsanKalıbı
    {
        public override İnsanKalıbı Klonla()
        {
            Console.WriteLine("{0} isimli kadının klonu oluşturuluyor", this.isim);
            return new Kadın()
            {
                isim = this.isim
            };
        }
    }


    class PrototypePattern
    {
      public static void Main(string[] args)
        {
            Erkek e = new Erkek();
            e.isim = "Koray";
            Console.WriteLine(e.isim);
            Erkek eKlon = e.Klonla() as Erkek;
            Console.WriteLine(eKlon.isim);

        }

    }
}
