﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace State
{
    interface IKavgayaKarışabilir
    {
        void KavgayaKarış(Mazlum Kader);
    }

    class Mazlum
    {
        public IKavgayaKarışabilir KavgaMod;
        public Mazlum(IKavgayaKarışabilir KavgaMod)
        {
            this.KavgaMod = KavgaMod;
        }
        public int tipNo;

        public void YaşamayaÇalış()
        {
            KavgaMod.KavgayaKarış(this);
        }
    }

    class Ayık : IKavgayaKarışabilir
    {
        public void KavgayaKarış(Mazlum Kader)
        {
            Console.WriteLine("Ayıkken kavgaya karışılıyor.");
            Console.WriteLine("Karşıla ve yumruk at.");
            Kader.KavgaMod = new Sarhoş();
        }
    }

    class Sarhoş : IKavgayaKarışabilir
    {
        public void KavgayaKarış(Mazlum Kader)
        {
            Console.WriteLine("Sarhoşken kavgaya karışılıyor.");
            Console.WriteLine("Bayılana kadar dayak ye");
            Kader.KavgaMod = new Ayık();
        }
    }

    class StateDemo
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Ayık Modla başlanıyor");
            Mazlum mazlum = new Mazlum(new Ayık());
            while (true)
            {
                mazlum.YaşamayaÇalış();
                Console.WriteLine("Yaşamaya devam et(E H) ?");
                if (Console.ReadLine().Trim().ToLower() == "h")
                {
                    break;
                }
            }
        }
    }

}
