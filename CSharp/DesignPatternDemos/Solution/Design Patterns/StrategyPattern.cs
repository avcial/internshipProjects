﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy
{
    interface ISaldırabilir
    {
        void Saldır(int AskerSayısı);
    }


    class HilalTaktiği : ISaldırabilir
    {
        public void Saldır(int AskerSayısı)
        {
            int bölük = Convert.ToInt32(AskerSayısı / 3);
            Console.WriteLine("{0} asker soldan, {1} asker sağdan {2} asker ortadan saldırıyor.", bölük,bölük,bölük);
        }
    }

    class HücumTaktiği : ISaldırabilir
    {
        public void Saldır(int AskerSayısı)
        {
            Console.WriteLine("{0} asker hep birlikte düşmanın üstüne koşuyor.",AskerSayısı);
        }
    }

    class Ordu
    {
        public ISaldırabilir Taktik;
        int AskerSayısı;
        public Ordu(int AskerSayısı)
        {
            this.AskerSayısı = AskerSayısı;
        }
        public void VerMehteri()
        {
            Taktik.Saldır(this.AskerSayısı);
        }
    }

    class StrategyPatternDemo
    {
        public static void Main(string[] args)
        {
            int AskerSayısı;
            while (true)
            {
                Console.Write("Ordunuzun asker sayısını giriniz : ");
                string veri = Console.ReadLine();
                if (Int32.TryParse(veri,out AskerSayısı))
                {
                    break;
                }
            }
            

            Ordu AltınOrdu = new Ordu(AskerSayısı);
            AltınOrdu.Taktik = new HilalTaktiği();

            AltınOrdu.VerMehteri();

            AltınOrdu.Taktik = new HücumTaktiği();
            AltınOrdu.VerMehteri();
        }
    }
}
