import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by avcial on 28.06.2017.
 */
public class MainClass
{

    public static final String USERNAME = "avcial";
    public static final String ACCESS_KEY = "7a7258c7-0a97-46c7-988b-2478717e9f36";
    public static final String URL = "https://" + USERNAME + ":" + ACCESS_KEY + "@ondemand.saucelabs.com:443/wd/hub";

    public static void main(String[] args) throws Exception {

        WrongAuth();
    }


    public static void WrongAuth() throws MalformedURLException {

        DesiredCapabilities caps = DesiredCapabilities.chrome();
        caps.setCapability("platform", "Windows XP");
        caps.setCapability("version", "43.0");
        caps.setCapability("name","myTest");

        WebDriver driver = new RemoteWebDriver(new URL(URL), caps);
        driver.get("https://www.**.com.tr");

        WebElement weClientArea = driver.findElement(By.cssSelector("****"));



        WebDriverWait wait = new WebDriverWait(driver, 10);
        try {
            weClientArea.click();
            System.out.println("butona basıldı.");

            System.err.println("looking for username element (try)");
            WebElement clientAreaElement = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("username")));
        } catch (Exception e) {
            System.out.println("Catch");
            return;
        }
        System.out.println("Üye sayfasına girildi.");
        WebElement weUsername = driver.findElement(By.name("username"));
        WebElement wePassword = driver.findElement(By.name("password"));
        WebElement weForm = driver.findElement(By.tagName("form"));
        weUsername.sendKeys("***@****");
        wePassword.sendKeys("*****");
        weForm.submit();

        String messageText = "init" ;
        WebElement messageElement = null;
        try{
            System.out.println("Try başladı");
            messageElement = wait.until( ExpectedConditions.presenceOfElementLocated(By.cssSelector("#errors a")));

            System.out.println(messageElement.getText().length());
            messageText =  messageElement.getAttribute("innerHTML");

        }
        catch(TimeoutException e){
            System.out.println(String.format("Catchtesin "));
            System.out.println(e);
            messageText = "Catch";
        }

        System.out.println("MessageText :" + messageText);
        System.out.println("bitti");

        Assert.assertEquals(messageText, "×");

        String jobID = ((RemoteWebDriver)driver).getSessionId().toString();



        driver.quit();
    }



}
